package es.sanitas.calculator.controller.dto;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum OperationTypeViewDto {

  ADDITION("addition"),
  SUBTRACTION("subtraction");

  private final String id;

  @Override
  public String toString() {
    return this.id;
  }
}
