package es.sanitas.calculator.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.format.support.FormattingConversionServiceFactoryBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import com.fasterxml.jackson.databind.ObjectMapper;

import es.sanitas.calculator.controller.config.OperationTypeEnumConverter;
import es.sanitas.calculator.controller.dto.CalculatorResponseViewDto;
import es.sanitas.calculator.controller.mapper.CalculatorMapperImpl;
import es.sanitas.calculator.usecase.CalculatorUseCase;

@SpringJUnitConfig(classes = FormattingConversionServiceFactoryBean.class)
class CalculatorControllerTest {
  
  private final Double RESULT = 11.0;

  @Autowired
  FormattingConversionServiceFactoryBean conversionServiceFactoryBean;

  @Mock
  private CalculatorUseCase useCase;

  private MockMvc mockMvc;
  private ObjectMapper objectMapper;

	@BeforeEach
	void setUp() {
    final FormattingConversionService conversionService = conversionServiceFactoryBean.getObject();
    conversionService.addConverter(new OperationTypeEnumConverter());

    mockMvc = MockMvcBuilders.standaloneSetup(new CalculatorControllerImpl(useCase, new CalculatorMapperImpl()))
        .setCustomHandlerMapping(RequestMappingHandlerMapping::new)
        .setConversionService(conversionService)
        .build();
    objectMapper = new ObjectMapper();
	}

	@Test
  void getResultsTest() throws Exception {
    given(useCase.getResults(any(), any(), any())).willReturn(RESULT);

    final CalculatorResponseViewDto expected = new CalculatorResponseViewDto();
    expected.setResult(RESULT);
	  
    mockMvc
        .perform(get("/arithmetic-calculations").queryParam("firstNumber", "5").queryParam("secondNumber", "6")
            .queryParam("operation", "addition").accept(MediaType.APPLICATION_JSON_VALUE))
        .andExpect(status().isOk())
        .andExpect(content().json(objectMapper.writeValueAsString(expected)));
	}
}
