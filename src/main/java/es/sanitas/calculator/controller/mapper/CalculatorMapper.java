package es.sanitas.calculator.controller.mapper;

import org.mapstruct.Mapper;

import es.sanitas.calculator.controller.dto.CalculatorResponseViewDto;
import es.sanitas.calculator.controller.dto.OperationTypeViewDto;
import es.sanitas.calculator.model.OperationType;

@Mapper(componentModel = "spring")
public interface CalculatorMapper {

  OperationType mapOperation(OperationTypeViewDto operation);

  CalculatorResponseViewDto mapResult(Double result);
}
