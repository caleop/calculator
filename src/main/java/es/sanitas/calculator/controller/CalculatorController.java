package es.sanitas.calculator.controller;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import es.sanitas.calculator.controller.dto.CalculatorResponseViewDto;
import es.sanitas.calculator.controller.dto.OperationTypeViewDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

@Tag(name = "Calculator")
public interface CalculatorController {
	
  @GetMapping(path = "/arithmetic-calculations", produces = MediaType.APPLICATION_JSON_VALUE)
  @Operation(summary = "Retrieves the result of an arithmetic operation between two numbers",
      description = "Retrieves the result of an arithmetic operation between two numbers",
      parameters = {
          @Parameter(in = ParameterIn.QUERY, name = "firstNumber", description = "First number of the operation"),
          @Parameter(in = ParameterIn.QUERY, name = "secondNumber", description = "Second number of the operation"),
          @Parameter(in = ParameterIn.QUERY, name = "operation", description = "Type of the arithmetic operation")})
  CalculatorResponseViewDto getResults(@RequestParam Double firstNumber, @RequestParam Double secondNumber,
      @RequestParam OperationTypeViewDto operation);
}
