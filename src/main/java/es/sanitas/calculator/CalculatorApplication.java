package es.sanitas.calculator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackageClasses = {CalculatorApplication.class})
public class CalculatorApplication {

	public static void main(final String[] args) {
		SpringApplication.run(CalculatorApplication.class, args);
	}

}
