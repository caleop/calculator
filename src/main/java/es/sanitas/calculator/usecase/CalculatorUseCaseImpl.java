package es.sanitas.calculator.usecase;

import org.springframework.stereotype.Service;

import es.sanitas.calculator.model.OperationType;
import io.corp.calculator.TracerImpl;

@Service
public class CalculatorUseCaseImpl implements CalculatorUseCase {

  private final TracerImpl tracer = new TracerImpl();

  @Override
  public Double getResults(final Double firstNumber, final Double secondNumber, final OperationType operation) {

    Double result = null;

    switch (operation) {
      case ADDITION:
        result = firstNumber + secondNumber;
        break;
      case SUBTRACTION:
        result = firstNumber - secondNumber;
        break;  
      default:
        break;
    }

    tracer.trace(result);

    return result;
  }
}
