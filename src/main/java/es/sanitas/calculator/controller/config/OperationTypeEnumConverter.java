package es.sanitas.calculator.controller.config;

import java.util.stream.Stream;

import org.springframework.core.convert.converter.Converter;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import es.sanitas.calculator.controller.dto.OperationTypeViewDto;

public class OperationTypeEnumConverter implements Converter<String, OperationTypeViewDto> {

  @Override
  public OperationTypeViewDto convert(final String source) {
    return Stream.of(OperationTypeViewDto.values())
        .filter(operation -> operation.getId().equals(source))
        .findFirst()
        .orElseThrow(() -> new ResponseStatusException(HttpStatus.BAD_REQUEST));
  }
}
