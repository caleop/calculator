package es.sanitas.calculator;

import org.junit.jupiter.api.Test;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

@SpringJUnitConfig
class CalculatorApplicationTests {

  @Test
  void contextLoads() {
    CalculatorApplication.main(new String[] {});
  }

}
