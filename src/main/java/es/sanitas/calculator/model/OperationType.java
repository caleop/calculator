package es.sanitas.calculator.model;

public enum OperationType {
  ADDITION, SUBTRACTION;
}
