package es.sanitas.calculator.controller.config;

import static org.assertj.core.api.BDDAssertions.then;
import static org.assertj.core.api.BDDAssertions.thenThrownBy;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.web.server.ResponseStatusException;

import es.sanitas.calculator.controller.dto.OperationTypeViewDto;

class OperationTypeEnumConverterTest {

  private static final String BAD_OPERATION = "dummy";

  private OperationTypeEnumConverter converter;

  @BeforeEach
  void setUp() {
    converter = new OperationTypeEnumConverter();
  }

  @Test
  void convertTest() {
    then(converter.convert(OperationTypeViewDto.ADDITION.getId())).isEqualTo(OperationTypeViewDto.ADDITION);
    then(converter.convert(OperationTypeViewDto.SUBTRACTION.getId())).isEqualTo(OperationTypeViewDto.SUBTRACTION);
  }
  
  @Test
  void convertBadRequestExceptionTest() {
    thenThrownBy(() -> converter.convert(BAD_OPERATION))
        .isInstanceOf(ResponseStatusException.class);
  }

}
