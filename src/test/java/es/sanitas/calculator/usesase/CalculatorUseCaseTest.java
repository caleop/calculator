package es.sanitas.calculator.usesase;

import static org.assertj.core.api.BDDAssertions.then;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.sanitas.calculator.model.OperationType;
import es.sanitas.calculator.usecase.CalculatorUseCase;
import es.sanitas.calculator.usecase.CalculatorUseCaseImpl;

public class CalculatorUseCaseTest {

  private static final Double FIRST_NUMBER = Double.valueOf(10.5);
  private static final Double SECOND_NUMBER = Double.valueOf(4.1);

  private CalculatorUseCase useCase;
  
  @BeforeEach
  void setUp() {
    useCase = new CalculatorUseCaseImpl();
  }

  @Test
  void getResultsAdditionTest() {
    then(useCase.getResults(FIRST_NUMBER, SECOND_NUMBER, OperationType.ADDITION)).isEqualTo(14.6);
    then(useCase.getResults(FIRST_NUMBER, SECOND_NUMBER, OperationType.SUBTRACTION)).isEqualTo(6.4);
  }
}
