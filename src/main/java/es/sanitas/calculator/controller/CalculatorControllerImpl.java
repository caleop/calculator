package es.sanitas.calculator.controller;

import org.springframework.web.bind.annotation.RestController;

import es.sanitas.calculator.controller.dto.CalculatorResponseViewDto;
import es.sanitas.calculator.controller.dto.OperationTypeViewDto;
import es.sanitas.calculator.controller.mapper.CalculatorMapper;
import es.sanitas.calculator.usecase.CalculatorUseCase;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class CalculatorControllerImpl implements CalculatorController {

  private final CalculatorUseCase useCase;
  private final CalculatorMapper mapper;

  @Override
  public CalculatorResponseViewDto getResults(final Double firstNumber, final Double secondNumber,
      final OperationTypeViewDto operation) {

    return mapper.mapResult(useCase.getResults(firstNumber, secondNumber, mapper.mapOperation(operation)));
  }
}
