package es.sanitas.calculator.controller.mapper;

import static org.assertj.core.api.BDDAssertions.then;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import es.sanitas.calculator.controller.dto.CalculatorResponseViewDto;
import es.sanitas.calculator.controller.dto.OperationTypeViewDto;
import es.sanitas.calculator.model.OperationType;

public class CalculatorMapperTest {

  private static final Double RESULT = Double.valueOf(5);

  private CalculatorMapper mapper;

  @BeforeEach
  void setUp() {
    mapper = new CalculatorMapperImpl();
  }

  @Test
  void mapOperationTest() {
    then(mapper.mapOperation(OperationTypeViewDto.ADDITION)).isEqualTo(OperationType.ADDITION);
    then(mapper.mapOperation(OperationTypeViewDto.SUBTRACTION)).isEqualTo(OperationType.SUBTRACTION);
    then(mapper.mapOperation(null)).isNull();
  }

  @Test
  void mapResultTest() {
    final CalculatorResponseViewDto response = mapper.mapResult(RESULT);
    then(response.getResult()).isEqualTo(RESULT);
    then(mapper.mapResult(null)).isEqualTo(null);
  }
}
