# README #

Microservicio realizado con SpringBoot y Maven que implementa operaciones aritméticas básicas (actualmente suma y resta) entre dos operandos.

Pasos a seguir para compilar y ejecutar el microservicio:

```
git clone https://caleop@bitbucket.org/caleop/calculator.git
cd calculator
mvn validate
mvn clean install
```

Se debe haber generado un jar llamado calculator-0.0.1-SNAPSHOT.jar que podremos ejecutar.

```
cd target
java -jar calculator-0.0.1-SNAPSHOT.jar
```

Una vez levantado el microservicio en el host http://localhost:8080/
se puede acceder a su interfaz introducciendo la siguiente url en un navegador, para poder proceder a su prueba.

```
http://localhost:8080/swagger-ui/index.html
```
