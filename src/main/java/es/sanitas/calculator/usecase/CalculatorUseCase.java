package es.sanitas.calculator.usecase;

import es.sanitas.calculator.model.OperationType;

public interface CalculatorUseCase {
  Double getResults(Double firstNumber, Double secondNumber, OperationType operation);
}
