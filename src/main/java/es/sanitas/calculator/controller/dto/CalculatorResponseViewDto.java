package es.sanitas.calculator.controller.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(title = "CalculatorResponseViewDto", name = "calculatorResponseViewDto",
    description = "POJO which represents a CalculatorResponseViewDto")
public class CalculatorResponseViewDto {

  @Schema(description = "Calculation result", example = "5.0")
  private Double result;
}
